/*
UNIVERSITY OF TORONTO
Faculty of Applied Science and Engineering
Division of Engineering Science

CSC192 Computer Programming, Algorithms and Data Structures
Assignment 5: Huffman coding
Programmed by: Yeo, Yih Tang
*/

#include <stdio.h>
#include <stdlib.h>

//  ------------------------------------ Global Variable --------------------------------  //

// create a heap data structure to store the character (for identification purpose), and the priority of the character (for sorting purpose)
// the pointer "left" and "right" are not used when setting up the priority queue in Step 2, but only will be used in Step 3 when there is a parent needs to point towards two children
struct heap_priority
{
    int char_in_ascii;
    float priority;
    struct heap_priority* left;
    struct heap_priority* right;
};
struct heap_priority heap_node[128];        // create an array of the nodes in the heap. ASCII codes range from 0 to 127, in total of 128 characters
int num_heap_nodes = 0;                     // keep track the "valid" number of nodes in the heap as base case for loop to terminate
char* hash_code[128];                       // create a table of huffman hash code so that when the file is read again, it can find its huffman code immediately


//  ------------------------------------ List of Functions --------------------------------  //

void insertPriorityQueue(int, float, struct heap_priority*, struct heap_priority*);
// this function accepts: 1. the ascii code of the character in decimal, 2. the priority of the character, 3. the pointer to the left child, 4. the pointer to the right child

struct heap_priority getMaximumPriorityItem();
// this function will return the node with the greatest priority from the heap, which is the first element

// some simple algorithms to find parents, left child and right child of a node in a binary tree structure
int find_binary_tree_right_child(int);
int find_binary_tree_left_child(int);
int find_binary_tree_parent(int);

struct heap_priority* create_node(struct heap_priority);
// this function accepts a node from the heap and change it into a dynamic storage variable to be attached to a parent node

void readHuffmanNode(struct heap_priority*, char[], int);
// this function travels down the Huffman tree and assigning 0 when travel left and assigning 1 when travel right

// two functions: one is to add characters to the end of string, another is to remove the last character from the string
char* add_character_to_end_of_string(char* old_string, char ch_to_be_added, int currentLength);
char* shrink_string_by_one_char(char *old_string, int currentLength);

void storeIntoHashTable(int ascii, char *string, int strlength);
// this function accepts the ascii code in decimal form, the string of bits and the length of the string to be stored in a hash table to be refered again when the input file is read again

//  ------------------------------------ Begin of Program --------------------------------  //

int main()
{
    // create an array counting the frequency of each character
    int char_frequency[128] = {0};

    // calculate the total length of the file being read
    int total_frequency = 0;

    FILE *inputFile;
    char* fileName;
    char currentChar;

    printf("Please enter the file name: ");
    scanf("%s", fileName);

    inputFile = fopen(fileName, "r");

    // Error detection: If the file is invalid, print error and quit
    if (inputFile == NULL)
    {
        printf("Error: File unknown.\n");
        exit(EXIT_FAILURE);
    }

    // STEP 1: Create an array of size 128 (total number of ASCII characters) and keep track of the frequency as it goes through the whole input
    // note: the step 1 of counting the probability and priority is delayed to step 2 to prevent O(2n) to happen.
    do
    {
        currentChar = fgetc(inputFile);

        // read the file until it reaches the end
        if (currentChar == EOF)
            break;

        // implicitly convert the character into ASCII code to increment the respective index in the array
        char_frequency[(int)currentChar]++;
        total_frequency++;

    } while (currentChar != EOF);

    // STEP 2: Place it into a heap priority queue
    int i;
    for (i = 0; i < 128; i++)
    {
        // read through the 128 characters in the loop
        if (char_frequency[i] != 0)
        {
            // STEP 1 continuation: divide frequency/total frequency and subtract it by 1 to get the priority
            // set pointer to right and pointer to left to NULL, as they don't have any child for now
            insertPriorityQueue(i, 1.0f-((float)char_frequency[i]/(float)total_frequency), NULL, NULL);
        }
    }

    struct heap_priority max_priority1, max_priority2; // lowest probability, end up at the end of list

    // STEP 3: construct a Huffman Tree based on the current priority queue
    // as long as there is more than 2 nodes to be removed from heap, execute it
    while (num_heap_nodes >=  2)
    {
        // take two nodes out from the heap by calling this function
        max_priority1 = getMaximumPriorityItem();
        max_priority2 = getMaximumPriorityItem();

        // build the tree by converting the left and right node into a dynamic memory to be held by the parent node using pointers
        struct heap_priority *left_node = create_node(max_priority1);
        struct heap_priority *right_node = create_node(max_priority2);

        insertPriorityQueue(-1, 1-(1-max_priority1.priority+1-max_priority2.priority), left_node, right_node);
        // -1 indicates no character

    }

    // STEP 4: create a head with empty properties on top of the tree
    struct heap_priority head;

    // the last two nodes being extracted from the heap are the largest two, so, make the head pointer points to both of them
    head.left = &max_priority2;
    head.right = &max_priority1;
    head.priority = 0.0f;
    head.char_in_ascii = -1;

    // STEP 5: ASSIGN CODE FOR EACH CHARACTER USING AN ASCII TABLE
    // the length of string is set to be 1000 to take it really large number, but this is temporary and the memory will be freed immediately after assigning 0 and 1 bits
    char* temporary_huffman_code = malloc(sizeof(char)*1000);

    if (temporary_huffman_code == NULL)
    {
        printf("Error: Out of memory.\n");
        exit(EXIT_FAILURE);
    }
    readHuffmanNode(&head, temporary_huffman_code, 0);

    // free the memory after being used
    free(temporary_huffman_code);

    // STEP 6: Read the input again
    inputFile = fopen(fileName, "r");
    do
    {
        currentChar = fgetc(inputFile);

        // stop reading when reaches the end of file
        if (currentChar == EOF)
            break;

        // print out the huffman code by accessing the hash table in the form of an array
        printf("%s ", hash_code[(int)currentChar]);


    } while (currentChar != EOF);
    printf("\n");

    return 0;
}


void readHuffmanNode(struct heap_priority* x, char* huffman_code, int currentLength)
{
    // as long as the node is not empty
    if (x != NULL){

        // recursviely go to the left, while adding 0 to the end of the string of bits
        readHuffmanNode(x->left, add_character_to_end_of_string(huffman_code, '0', currentLength), currentLength+1);

        // while arriving at the parent node, remove the last char in the string of bits
        huffman_code = shrink_string_by_one_char(huffman_code, currentLength);

        // if the ASCII code is valid and the currentlength of string is larger than 0, put it into the hash table
        if (x->char_in_ascii >= 0 && x->char_in_ascii <= 127 && currentLength >= 0)
        {
            storeIntoHashTable(x->char_in_ascii, huffman_code, currentLength);
        }

        // recursively go to the right, while adding 1 to the end of the string of bits
        readHuffmanNode(x->right, add_character_to_end_of_string(huffman_code, '1', currentLength), currentLength+1);
    }
}


void storeIntoHashTable(int ascii, char *string, int strlength)
{
    // initialize the length of string in each element of the array. Note that this is a 2D array
    hash_code[ascii] = malloc(strlength*sizeof(string));

    if (hash_code[ascii] == NULL)
    {
        printf("Error: Out of memory\n");
        exit(EXIT_FAILURE);
    }
    int i;

    // copy the entire content of the string into the element in the array
    for (i = 0; string[i] != '\0'; i++)
    {
        hash_code[ascii][i] = string[i];
    }
    // add an whitespace at the end
    hash_code[ascii][strlength] = '\0';
}


char* shrink_string_by_one_char(char *old_string, int currentLength)
{
    // add the null character at the (length)th index of the string to mske the string shorter by 1
    *(old_string+currentLength) = '\0';
    return old_string;
}

char* add_character_to_end_of_string(char* old_string, char ch_to_be_added, int currentLength)
{
    // add the character to be added (either 0 or 1) at the (length)th index of the string, and add a null behind it to mark an end to the string.
    *(old_string+currentLength) = ch_to_be_added;
    *(old_string+currentLength+1) = '\0';
    return old_string;
}

struct heap_priority* create_node(struct heap_priority heap_node)
{
    // create a new struct from the free memory by using malloc
    struct heap_priority * pointer_to_node = malloc(sizeof(struct heap_priority));

    if (pointer_to_node == NULL)
    {
        printf("Error: Out of memory.\n");
        exit(EXIT_FAILURE);
    }

    // copy the content of the original node into the newly created node
    *pointer_to_node = heap_node;

    return pointer_to_node;
}

struct heap_priority getMaximumPriorityItem()
{
    // the largest priority is always on top, so grab the one with index 0
    struct heap_priority max_priority_item = heap_node[0];
    num_heap_nodes--;

    // to reorder the heap, place the smallest node (which is the last node) on top of the tree and allow it to be sorted down the tree
    heap_node[0] = heap_node[num_heap_nodes];

    // delete the top one, move the smallest one to the top, check from top to bottom
    int left_child_index, right_child_index, parent_index = 0;

    // create a temporary node to be used during exchange between two nodes
    struct heap_priority temporary_heap_node;
    // find the left child and the right child of the node
    left_child_index = find_binary_tree_left_child(parent_index);
    right_child_index = find_binary_tree_right_child(parent_index);

    // if the left and right child exists, ie. they are not beyond the bound of the heap, execute the sorting loop
    while (left_child_index < num_heap_nodes && right_child_index < num_heap_nodes && left_child_index > 0 && right_child_index > 0)
    {
        // compare the right child with the parent. The parent node should be larger, and if it is not so, exchange the two nodes
         if (heap_node[right_child_index].priority > heap_node[parent_index].priority)
        {
            temporary_heap_node = heap_node[right_child_index];
            heap_node[right_child_index] = heap_node[parent_index];
            heap_node[parent_index] = temporary_heap_node;

            // now the parent is the right child, as we want to repeat the checking till the end of the tree
            parent_index = right_child_index;
        }
        else{
            // since the right child is always bigger than the left child, if the parent node is already larger than the right node,
            // there is no need to check by going down the tree anymore
            break;
        }

        // then check if the right child is bigger or smaller than the left child. If the left one is larger, swap with the right child
        if (heap_node[left_child_index].priority >= heap_node[right_child_index].priority)
        {
            temporary_heap_node = heap_node[right_child_index];
            heap_node[right_child_index] = heap_node[left_child_index];
            heap_node[left_child_index] = temporary_heap_node;

            // now the parent is the left child, as we want to repeat the checking till the end of the tree
            parent_index = left_child_index;
        }

        // in the case where the left is the largest, swap with the parent then
        if (heap_node[left_child_index].priority > heap_node[parent_index].priority)
        {
            temporary_heap_node = heap_node[left_child_index];
            heap_node[left_child_index] = heap_node[parent_index];
            heap_node[parent_index] = temporary_heap_node;

            // now the parent is the left child, as we want to repeat the checking till the end of the tree
            parent_index = left_child_index;
        }
        else{
            break;
        }

        // after having a "new" parent node, now, find the new left and right child and repeat the sorting
        left_child_index = find_binary_tree_left_child(parent_index);
        right_child_index = find_binary_tree_right_child(parent_index);
    }

    return max_priority_item;
}

void insertPriorityQueue(int char_ascii, float prob, struct heap_priority* leftPointer, struct heap_priority* rightPointer)
{
    // insert the "properties" of a character into the data structure
    heap_node[num_heap_nodes].char_in_ascii = char_ascii;
    heap_node[num_heap_nodes].priority = prob;
    heap_node[num_heap_nodes].left = leftPointer;
    heap_node[num_heap_nodes].right = rightPointer;

    // create two variables which are the index for the parent node and the child node
    int parent_index, child_index;

    // since we are adding a new node into the heap, increment the counter num_heap_nodes
    num_heap_nodes++;

    // the child node is the new node (num_heap_nodes-1 represents the last node)
    child_index = num_heap_nodes-1;
    // using the algorithm, find the parent of the node
    parent_index = find_binary_tree_parent(child_index);

    struct heap_priority temporary_heap_node;

    // using a while loop, check if the child is larger than the parent until the top of the tree, which is, when child index is the same as parent index
    while (parent_index != child_index)
    {
        // if the child is smaller than parent (which violates the condition), swap it with the parent
        if (heap_node[child_index].priority > heap_node[parent_index].priority)
        {
            temporary_heap_node = heap_node[child_index];
            heap_node[child_index] = heap_node[parent_index];
            heap_node[parent_index] = temporary_heap_node;
        }

        // if the right node exists, compare them
        if (find_binary_tree_right_child(parent_index) < num_heap_nodes)
        {
            // the left child must be smaller than the right, if not, swap it
            if (heap_node[find_binary_tree_left_child(parent_index)].priority > heap_node[find_binary_tree_right_child(parent_index)].priority)
            {
                temporary_heap_node = heap_node[find_binary_tree_left_child(parent_index)];
                heap_node[find_binary_tree_left_child(parent_index)] = heap_node[find_binary_tree_right_child(parent_index)];
                heap_node[find_binary_tree_right_child(parent_index)] = temporary_heap_node;
            }
        }

        // make the child as a parent and keeps moving up the tree while repeating the loop
        child_index = parent_index;
        // the parent of the new parent
        parent_index = find_binary_tree_parent(parent_index);
    }
}

// algorithm to find parent node
int find_binary_tree_parent(int node)
{
    if (node == 0)
        return 0;
        // this is made to return 0 so that we will be able to compare when it reaches the top (child = 0, parent = 0)
    else
        return (node-1)/2;
}
// algorithm to find left child node
int find_binary_tree_left_child(int node)
{
    return node*2+1;
}
// algorithm to find right child node
int find_binary_tree_right_child(int node)
{
    return node*2+2;
}
